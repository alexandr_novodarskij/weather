angular.module('weatherApp', ['ui.grid'])
  .constant('Config', {
      serverAddress : "http://127.0.0.1:3000",
  })
  .factory('DataFactory', function ($http, Config) {
      var DataFactory = {};

      DataFactory.getFile = function(city){
        if (typeof city == "undefined") {
          city = ""
        }
        return $http.get(Config.serverAddress+"/data/"+city)
      }
      return DataFactory;
  })
  .controller('MainCtrl', function($scope, DataFactory) {
    $scope.stuff = {meteoData:[]}
    $scope.customCity = "Bradford"
    $scope.getData = function () {
      DataFactory.getFile($scope.customCity)
      .then(
        function (res) {
          if (res.data.status) {
            $scope.stuff = res.data.result;
          }else{
            $scope.err = res.data.message;
            $scope.stuff = {meteoData:[]}
          }
        },
        function (err) {
          console.log(err);
        })
    }
    $scope.getData()
  });
