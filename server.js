var request = require('request');
cors = require('cors');
var express = require('express');
var app = express();
app.use(cors())
app.get('/',function (req, res) {
  res.sendFile( __dirname+  "/index.html")
})
app.use('/app', express.static('./app'))
app.get('/data/:city', function (req, res) {
  var city = ''
  if (typeof req.params.city != 'undefined') {
    city = req.params.city//.toLowerCase();
    if (city.split(" ").length > 1) {
      city = city.split(" ")[0].toLowerCase();
    }else {
      city = city.toLowerCase();
    }
  }
  request.get('http://www.metoffice.gov.uk/pub/data/weather/uk/climate/stationdata/'+city+'data.txt', function (error, response, body) {
      if (!error && response.statusCode == 200) {
          var data = body.split("\n");
          var result = {
            meteoData:[],
            city:data[0],
            location:data[1]
          }
          for (var i = data.indexOf("              degC    degC    days      mm   hours")+1; i < data.length; i++) {
            var item = data[i].split(" ").filter(function(n){ return n != "" });
            result.meteoData.push({
              year :item[0],
              month:item[1],
              tmax :item[2],
              tmin :item[3],
              af   :item[4],
              rain :item[5],
              sun  :item[6],
            })
          }
          res.send(JSON.stringify({status:1,result:result}));
      }else {
        res.send({status:0,data:[],message:"Not Found"})
      }
  });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
