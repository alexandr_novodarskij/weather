# README #

Display a history of weather trends from a Meteorological Weather Station.

### What is this repository for? ###

 * Test task for Yuriy Berdnikov  

### How do get set up? ###

* git clone <repo>
* cd path/to/project
* npm install
* forever/node server.js
* open in browser 127.0.0.1:3000